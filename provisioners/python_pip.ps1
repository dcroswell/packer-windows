﻿# Download Python $env:PYTHON_VERSION
$py_source = "https://www.python.org/ftp/python/$env:PYTHON_VERSION/python-$env:PYTHON_VERSION.amd64.msi"
$py_destination = "c:\Users\Administrator\Downloads\python-$env:PYTHON_VERSION.amd64.msi"
Invoke-WebRequest $py_source -OutFile $py_destination
Write-Output "Python $env:PYTHON_VERSION has been downloaded"

# Include logs file and do a quiet install (no window interface)
$arguments = '/I ' + $py_destination + ' /quiet /l*v c:\py_log.log'
Start-Process msiexec.exe -Wait -ArgumentList $arguments
Write-Output "Python $env:PYTHON_VERSION has been silently installed"

# Include Python in the System Path Variable
$oldPath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
$pathToAdd = ";C:\$env:PYTHON_FOLDER\;C:\$env:PYTHON_FOLDER\Scripts\"
$newPath = $oldPath + $pathToAdd
Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
$env:Path = $newPath
Write-Output "Updated System Variables to contain: $newPath"

# Include Python in the Environment Path Variable
$oldPath = $env:Path
$newPath = $oldPath + $pathToAdd
$env:Path = $newPath
Write-Output "Updated Environment Varialbes to contain: $newPath"

# The first creates a file association
$_cmd1 = 'assoc .py=py_auto_file'
cmd.exe /c "`"$_cmd1`""
Write-Output "Python file association created"

# The second sets the program used to execute the file
$_cmd2 = 'ftype py_auto_file="C:\'
$_cmd2 += $env:PYTHON_FOLDER
$_cmd2 += '\python.exe" "%1" %*'
cmd.exe /c "`"$_cmd2`""
Write-Output "Python files associated with .exe"

# Download PIP
$pip_source = "https://bootstrap.pypa.io/get-pip.py"
$pip_destination = "c:\Users\Administrator\Downloads\get-pip.py"
Invoke-WebRequest $pip_source -OutFile $pip_destination
Write-Output "Pip has been downloaded"

# Install PIP with python command
python c:\Users\Administrator\Downloads\get-pip.py
Write-Output "Pip has been installed with get-pip"
