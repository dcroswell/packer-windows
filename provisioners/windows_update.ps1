Write-Host "Triggering windows update shceduled task..."
Start-ScheduledTask TriggerWindowsUpdate
Write-Host "Start-ScheduledTask result: $?"

Get-ScheduledTask -TaskName TriggerWindowsUpdate | Get-ScheduledTaskInfo

While ($true) {
    $state = (Get-ScheduledTask -TaskName TriggerWindowsUpdate).State
    Write-Host "Windows update in progress. Status: $state"

    if ($state -ieq "Running") {
        Start-Sleep 15
    }
    else {
        break
    }
}

Write-Host "-----------------------------------"
Get-ScheduledTask -TaskName TriggerWindowsUpdate | Get-ScheduledTaskInfo
Write-Host "-----------------------------------"

Write-Host "Windows update log"
Write-Host "-----------------------------------"
Get-Content C:\PSWindowsUpdate.log
Write-Host "-----------------------------------"
