# Function to convert the object to JSON
function ConvertTo-Json20([object] $item){
    add-type -assembly system.web.extensions
    $ps_js=new-object system.web.script.serialization.javascriptSerializer
    return $ps_js.Serialize($item)
}

# Function to convert the object from JSON to Object
function ConvertFrom-Json20([object] $item){
    add-type -assembly system.web.extensions
    $ps_js=new-object system.web.script.serialization.javascriptSerializer

    #The comma operator is the array construction operator in PowerShell
    return ,$ps_js.DeserializeObject($item)
}

write-output "-----Load the JSON into an dictionary object"
$a = ConvertFrom-Json20(Get-Content 'C:\Program Files\Amazon\SSM\Plugins\awsCloudWatch\AWS.EC2.Windows.CloudWatch.json' )

write-output "-----Modify the object.Region for region $env:AWS_REGION"
$a.EngineConfiguration.Components | % {if($_.Parameters.Region){ $_.Parameters.Region=$env:AWS_REGION }
}

write-output "-----Convert the object back to JSON and upload the file"
$output = ConvertTo-Json20($a)
set-content -Path 'C:\Program Files\Amazon\SSM\Plugins\awsCloudWatch\AWS.EC2.Windows.CloudWatch.json' -Value $output

write-host "(host) Restart the SSM service"
write-output "Restart the SSM service"
Restart-Service AmazonSSMAgent
