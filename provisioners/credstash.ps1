# This provisioner assumes python, pip and awscli are installed

# Use pip to install credstash
$_cmd = 'pip install credstash'
cmd.exe /c "`"$_cmd`""
Write-Output "Credstash has been installed with pip"
