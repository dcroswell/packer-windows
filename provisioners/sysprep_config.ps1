# Sysprep Config

$SettingsFile="C:\\Program Files\\Amazon\\Ec2ConfigService\\sysprep2008.xml"
$xml = [xml](get-content $SettingsFile)
[System.Xml.XmlNamespaceManager] $nsm = new-object System.Xml.XmlNamespaceManager $xml.NameTable
$nsm.AddNamespace("unattend", "urn:schemas-microsoft-com:unattend")
$specializeSettings = $xml.SelectSingleNode("//unattend:settings[@pass='specialize']", $nsm)
$component = $specializeSettings.SelectSingleNode("unattend:component[@name='Microsoft-Windows-Shell-Setup']", $nsm)

foreach ($element in $component.ChildNodes) {
  if ($element.name -eq "CopyProfile") {
    $element."#text" = "false"
  }
}

$xml.Save($SettingsFile)
