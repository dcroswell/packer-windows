﻿# Dowload Puppet $env:PUPPET_VERSION
# https://downloads.puppetlabs.com/windows/puppet-agent-$env:PUPPET_VERSION-x64.msi

Write-Output "Install Puppet Agent $env:PUPPET_VERSION with PowerShell"
$pp_source = "https://downloads.puppetlabs.com/windows/puppet-agent-$env:PUPPET_VERSION-x64.msi"
$pp_destination = "c:\Users\Administrator\Downloads\puppet-agent-$env:PUPPET_VERSION-x64.msi"
Invoke-WebRequest $pp_source -OutFile $pp_destination
Write-Output "Puppet Agent $env:PUPPET_VERSION has been downloaded"

# Include logs file and do a quiet install (no window interface)
$arguments = '/I ' + $pp_destination + ' /quiet /l*v c:\pp_log.log'
Start-Process msiexec.exe -Wait -ArgumentList $arguments
Write-Output "Puppet Agent $env:PUPPET_VERSION has been silently installed"
