
# This provisioner assumes Git is installed

# Download GO $env:GO_VERSION
$go_source = "https://storage.googleapis.com/golang/go$env:GO_VERSION.windows-amd64.msi"
$go_destination = "c:\Users\Administrator\Downloads\go$env:GO_VERSION.windows-amd64.msi"
Invoke-WebRequest $go_source -OutFile $go_destination
Write-Output "GO $env:GO_VERSION has been downloaded"

# Include logs file and do a quiet install (no window interface)
$arguments = '/I ' + $go_destination + ' /quiet /l*v c:\go_log.log'
Start-Process msiexec.exe -Wait -ArgumentList $arguments
Write-Output "GO $env:GO_VERSION has been silently installed"

# Setup the GO Path variable
$env:GOPATH = "c:\Go\bin\"
Write-Output "GOPATH has been set to $env:GOPATH"

# Include Go in the Environment Path Variables
$oldPath = $env:Path
$pathToAdd = ";c:\Go\bin\"
$newPath = $oldPath + $pathToAdd
if($oldPath -NotLike "*" + $pathToAdd + "*") {
  $env:Path = $newPath
  Write-Output "Updated Environment Varialbes to contain: $newPath"}

# Install Telegraf
go get github.com/influxdata/telegraf
Write-Output "Telegraf has been installed with GO"
