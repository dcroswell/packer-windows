# Packer Template for Windows Server 2012 R2

## Pre-requisites
####Packer
* Packer (https://packer.io/)
* To check if Packer is installed open a bash window and type `packer --version`
* Installation instructions (https://www.packer.io/intro/getting-started/install.html)
####AWS Account
* AWS credentials 
* Go to your ~/.aws folder and open the config file
* Make sure the default access key and id are set for the account you are trying to build in

## Project Structure
* `windows_soe.json` - Packer template for building a base Windows Server 2012 R2 image with latest windows update applied.
  * `provisioners` - contains supporting provisioner scripts written in powershell
  * `userdata` - contains userdata scripts that run during the baking process on startup
  * `variables` - contains default configuration data that can be overridden in the packer build command

## Building Base Windows Server 2012 R2 AMI

The process of building a base Windows Server 2012 R2 AMI is as follow:

* Instantiate an instance based on Amazon Windows Server 2012 R2 AMI
* `EC2SetPassword` plugin is enabled to allow collection of password via AWS Console
* Install Chocolatey -- package manager for windows
* Enable windows updates
* Apply latest windows updates (at the time of building)
* Disable windows updates
* Restart to ensure all updates are installed correctly
* Install required software (git, python, aws_tools, telegraf, puppet, cloudwatch)
* Disable Windows Updates
* Build AMI and register it in AWS
* Everything was built from scratch except the provisioners/PSWindowsUpdate/* and the files that call them.

## Tell Packer which image to build by specifying a builder
To do this you will need to specify which builder along with any variables. 
In this case you will build `default` builder and pass in the variables `vpc_id` and `subnet_id` For example, if you wanted to run the builder without debugging you would type:

```
packer build -debug -only=default -var-file=./variables/common.json -var 'aws_vpc_id=vpc-4675fb23' -var 'aws_subnet_id=subnet-5c394d39' -var 'aws_source_ami=ami-f3203190' windows_soe.json
```

NOTE: Remember that the base AMI specified must be available in the region you are building in. Check the variables/common.json to see where it will be built.

NOTE: The default provisioners currently set should allow WinRM to be set up for communication. If you do not run these, you will not be able to WinRm to the machine.

## Handy debugging hint (as discussed) 
### The NULL Builder

You can use the null builder for testing the provisioners individually, without having to create a new instance each time.

The windows_soe.json file should have a builder with the name `default`.

```json
{
    "name": "default",
    "type": "amazon-ebs",
    "region": "{{ user `aws_region` }}",
    "source_ami": "{{ user `aws_source_ami` }}",
    "instance_type": "{{ user `aws_instance_type` }}",
    "ami_name": "{{ user `os_version` }}-{{ user `application` }}-{{ user `soe_version` }}-{{ user `role` }}-{{ user `build_number` }}-{{ isotime | clean_ami_name }}",
    "ami_description": "Windows server 2012 R2 with latest windows updates applied",
    "associate_public_ip_address": true,
    "communicator": "winrm",
    "winrm_username": "{{ user `winrm_username` }}",
    "vpc_id": "{{ user `vpc_id` }}",
    "subnet_id": "{{ user `subnet_id` }}",
    "user_data_file": "./userdata/setup_winrm.ps1",
    "ami_block_device_mappings": [
    {
      "device_name": "/dev/sda1",
      "volume_type": "standard",
      "volume_size": "100",
      "delete_on_termination": "true"
    }]
}
```


and a builder with the name `null`.

```json
{
  "name": "null",
  "type": "null",
  "communicator": "winrm",
  "winrm_use_ssl": true,
  "winrm_insecure": true,
  "winrm_host": "{{ user `winrm_host` }}",
  "winrm_username": "{{ user `winrm_username` }}",
  "winrm_password": "{{ user `winrm_password` }}"
}
```

The idea is to run the default builder until you get to the provisioning step. Then run the null builder against the default instance, on each provisioner. This allows you to test the provisioner on an instance that will be in the same state without having to bring up a new box each time you want to modify the image.

First, run the packer command in `debug mode` and ensure that the `only` parameter is set to `default`.

 ```bash
 packer build -only=default -var-file=./variables/common.json -var 'aws_vpc_id=vpc-4675fb23' -var 'aws_subnet_id=subnet-5c394d39' -var 'aws_source_ami=ami-f3203190' windows_soe.json
 ```
 
 NOTE: Remember that the base AMI specified must be available in the region you are building in. Check the variables/common.json to see where it will be built.

The debug option used above will require Packer to stop after every step. You will need to hit enter when you are ready to move forward. 

The goal is to get an instance to the point where packer can run test provisioners on it. (instance is up and running in the cloud)

 1. Open a bash window and type the command above. Keep pressing 'enter' until you get to 'Pausing after run of step 'StepConnect'.
The connect step tests the connection to the instance. NOTE: Remember to take down the Host IP address and the password generated by the setup process.

2. At this stage you can stop if you don't need any of your provisioners to load first, otherwise continue until you get to 'StepProvision', then *STOP!!*NOTE:
Provisioners that have the only flag will only run on that build. Provisioners with no `only` set will run on all builders.

3. Configure your `null builder` in the packer template (windows_soe.json) with the communicator variables taken down in step 1 above.
The `user` variables referenced in the json below are set in the varibles section at the top of the template.
```json
{
  "type": "null",
  "name": "null",
  "communicator": "winrm",
  "winrm_use_ssl": true,
  "winrm_insecure": true,
  "winrm_host": "{{ user `winrm_host` }}",
  "winrm_username": "Administrator",
  "winrm_password": "{{ user `winrm_password` }}"
}
```

4. Open a second bash window the same directory and type the following:

```bash
packer build -only=null -var-file=./variables/common.json -var 'aws_vpc_id=vpc-4675fb23' -var 'aws_subnet_id=subnet-5c394d39' -var 'aws_source_ami=ami-f3203190' windows_soe.json
```

NOTE: Notice the -only parameter is set to the `null` builder and the extra variables of `winrm_password` and `winrm_host` are referenced in the template at the top of the template. These can be overridden by passing in the values as variables'. 
Also be aware that when running this command, all provisioners flagged to run only with null 'only": ["null"]' along with ALL of the provisioners with no specification, will run with this command. It is suggested to specify the only tag on all provisioners when testing.

5. Test your provisioners one at a time without spinning up a new instance each time. :)
Keep in mind that the null process does not restore your process to the original state it was in with the first time you ran a null provisioner. Once you have run a null provisioner, it can't be reverted back.

###Debugging
1. WinRm Issues - if you have trouble connecting, take a look in userdata at the last few lines of the setup_winrm.ps1. Make sure this traffic can get from your machine to the instance on the ports specified)

2. Try running in a fresh account with the default vpc first if there are issues. This was tested there and the image was built. (I did not run all the provisioners to save time)

3. The disable_stop_instance in the windows_soe.json file is set to true for the sysprep provisioner. This means you must manually stop the instance to create the image or set the disable_stop_instance to false.